# Round Trip

## TODO

Just try something: Change your website, the CSS, include something from somewhere else .... There are numerous possibilities.

Nothing to copy now, do what you want to do. But do at the end the normal workflow: commit, push, wait, see the result on the web.

Get in touch with the workshop leader how the result can be seen live ...

## Result

You should expect the following:

* After having pushed your changes, the build is done automatically.
* Aftersome time, you should see all stages succeed.
* After the last stage, your deployment will be triggered, so the change in web site should be visible to you.