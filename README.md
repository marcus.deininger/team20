# DevOpsWorkshop

This repository is just a starting point to have something to play with. People in the workshop will clone it, and then change at will.

## Public `Playgrounds`

* Docker: https://labs.play-with-docker.com/ (Login with Docker)
* Kubernetes (K8s): https://labs.play-with-k8s.com (Login with Docker or Github)

If you want to get your feets wet, and try it out in a native environment. Alternative is, to have it installed locally.

* Easy for Linux: Install Docker from your software manager (apt, yum, ...)
* Mac OS-X: https://docs.docker.com/docker-for-mac/install/ (never tried that)
* Windows: https://docs.docker.com/docker-for-windows/install/ (or use VirtualBox and inside a Linux VirtualBox image then Docker)

## Plans

1. Start with a core build process, with nearly no functionality.
1. Introduce the build, with errors.
1. Complete the build the first time, with a docker image as result.
1. Push the image to the docker registry.
1. Deploy the image somehow.
1. Change something so that the docker image is changed as well (after another deployment).
1. Scale the deployment.

Each of those steps gets its own directory, which is located under plans. You have to copy the files, or create them on your own, if you want.

## Initial Setup for Each Team

1. Start by using the base URL for your project, e.g. https://gitlab.com/devopshft/team01.git
1. Do a `git clone <base URL>` in a new directory. (There will be an additional setup necessary to do a push later, will be told in the workshop only)
1. Have a look in the plan directories for the steps to do.

## Cheat Sheets

To make the exercises on your own, you have to have a basic understanding how Git, Docker and Kubernetes work. Here is a list of cheatsheets for details.

* Docker
  * https://github.com/wsargent/docker-cheat-sheet Very detailed, ...
  * https://devhints.io/docker Very dense, perfect for looking up commands
* Kubernetes
  * https://kubernetes.io/docs/reference/kubectl/cheatsheet/ More a reference (everything you could possibly know ...)
  * https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands Detailed documentation to all commands of `kubectl`.
* Git
  * https://www.git-tower.com/blog/git-cheat-sheet
  
## Base steps in all exercises

* `git clone <URL>` (given in the excercise)
* `cd <DIR>` (depends on the cloned repository)
* `more plan<NR>/README.md` (or look it up in the UI)
* Do some changes in the directory itself, or in `src`.
* `git add -A` (only necessary for completely new files)
* `git commit -a -m"<your message>"` (commit all changes)
* `git push` (Push the changes to the central repository)
* Wait for the build server to finish its job. The build server in our exercises is Gitlab-CI, but should work similar with Jenkins, TravisCI, Circle-CI, ...


